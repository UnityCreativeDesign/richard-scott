﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using System.IO;
public class JsonManager : MonoBehaviour
{
    public string localPath;
    public ShapeContainer shapes;
    void Start()
    {
        string[] jsonDataArr = File.ReadAllLines(Path.Combine(Application.dataPath, localPath));
        string jsonData = "";
        foreach(string st in jsonDataArr)
        {
            jsonData += st;
        }

        shapes = JsonUtility.FromJson<ShapeContainer>(jsonData);

        foreach(ShapeClass s in shapes.spawnObjects)
        {
            createObject(s);
        }
     
    }

    /// <summary>
    /// Creates an object in the world based on the shape class passed in
    /// </summary>
    /// <param name="toCreate">The shape class to create the object from</param>
    void createObject(ShapeClass toCreate)
    {
        PrimitiveType typeToCreate = PrimitiveType.Cube;

        switch (toCreate.primitive)
        {
            case "Sphere":
                typeToCreate = PrimitiveType.Sphere;
                break;
            case "Capsule":
                typeToCreate = PrimitiveType.Capsule;
                break;
            case "Cube":
                typeToCreate = PrimitiveType.Cube;
                break;
            case "Plane":
                typeToCreate = PrimitiveType.Plane;
                break;
            case "Quad":
                typeToCreate = PrimitiveType.Quad;
                break;
            case "Cylinder":
                typeToCreate = PrimitiveType.Cylinder;
                break;
            default:
                typeToCreate = PrimitiveType.Cube;
                break;
        }

        GameObject g = GameObject.CreatePrimitive(typeToCreate);
        g.name = toCreate.name;
        g.transform.position = toCreate.position;
        g.transform.rotation = Quaternion.Euler(toCreate.rotation);
        g.transform.localScale = toCreate.scale;
        g.AddComponent<ShapeInformationDisplay>().setValues(toCreate.information);

    }
   
}

[System.Serializable]
public class ShapeContainer
{
   public ShapeClass[] spawnObjects;
}

[System.Serializable]
public class ShapeClass
{
    public string name;
    public Vector3 position, rotation, scale;
    public string primitive;
    public ShapeInformation information;
}

[System.Serializable]
public class ShapeInformation
{
    public string stringField;
    public int intField;
    public float floatField;
    public bool boolField;
}
