﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Strings : MonoBehaviour
{
    public string toTest = "";
    
    void Update()
    {
        if (isPalindrome(toTest))
        {
            Debug.Log(toTest + " is a palindrome");
        }
        else
        {
            Debug.Log(toTest + " is not a palindrome");
        }
    }

   /// <summary>
   /// Function that checks whether the string passed in is a palindrome by checking the ends of the string and working inwards
   /// </summary>
   /// <param name="toCheck">The string to test</param>
   /// <returns>Returns false if the string is not a palindrome, else returns true</returns>
    bool isPalindrome(string toCheck)
    {
        string lowerCase = toCheck.ToLower();
        char[] chars = lowerCase.ToCharArray();

        for(int x = 0; x < chars.Length / 2; x++)
        {
            if (chars[x] != chars[(chars.Length - 1) - x])
            {
                return false;
            }
        }
        return true;
    }
}
