﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
public class Numbers : MonoBehaviour
{
    private void Start()
    {
        readInFile();
        printAllPrimeNumbers(numbers);
        printMultiples(5, numbers);
        arrangeNumbers(ref numbers);
    }

    public string localFilePath;
    public int[] numbers;
    void readInFile()
    {
        string path = Path.Combine(Application.dataPath, localFilePath);
        StreamReader sr = new StreamReader(path);
        string st = sr.ReadLine();
        List<string> rawData = new List<string>();
        while(st!=null)
        {
            rawData.Add(st);
            st = sr.ReadLine();
        }

        List<int> converted = new List<int>();
        foreach(string data in rawData)
        {
            converted.Add(int.Parse(data));
        }

        numbers = converted.ToArray();
    }

    /// <summary>
    /// Sorts a given array of integers into numerical order.
    /// </summary>
    /// <param name="toArrange">The array to sort, </param>
    void arrangeNumbers(ref int[] toArrange)
    {
        string st = "";
        for (int x = 0; x < toArrange.Length; x++)
        {
            st += toArrange[x] + ",";
        }
        Debug.Log("Unsorted list is " + st);
        while (isListInOrder(toArrange) == false)
        {
            for (int x = 0; x < toArrange.Length - 2; x++)
            {
                if (toArrange[x] > toArrange[x + 1])
                {
                    int temp = toArrange[x];
                    toArrange[x] = toArrange[x + 1];
                    toArrange[x + 1] = temp;
                }
            }
        }
        st = "";
        for (int x = 0; x < toArrange.Length; x++)
        {
            st += toArrange[x] + ",";
        }
        Debug.Log("Sorted list is " + st);
    }

    /// <summary>
    /// Tests for whether a given list is in order by taking numbers two at a time and making sure the first is less/equal to the the second, if not it returns false
    /// </summary>
    /// <param name="toCheck">The list of integers to check</param>
    /// <returns>False if list not in order, else true</returns>
    bool isListInOrder(int[] toCheck)
    {
        for (int x = 0; x < toCheck.Length - 2; x++)
        {
            if (toCheck[x] > toCheck[x + 1])
            {
                return false;
            }
        }
        return true;
    }

    //Goes through a list and checks if a division between each number in the list and the number to check is a whole number, if it is a whole number then the number in the list is a multiple
    void printMultiples(int num, int[] toCheck)
    {
        for (int x = 0; x < toCheck.Length; x++)
        {
            if (toCheck[x] == 0)
            {
                continue;
            }

            if (toCheck[x] % num == 0)
            {
                Debug.Log(toCheck[x] + " is a multiple of " + num);
            }

        }
    }

    /// <summary>
    /// Takes a list of integers and prints which ones are prime numbers
    /// </summary>
    /// <param name="toCheck"></param>
    void printAllPrimeNumbers(int[] toCheck)
    {
        for (int x = 0; x < toCheck.Length; x++)
        {
            if (isNumberPrime(toCheck[x]))
            {
                Debug.Log(toCheck[x] + " is a prime number");
            }

        }
    }

    /// <summary>
    /// Tests to see if a number is prime, does this by testing if the number, when divided by any number from its square root to 2 results in a  whole number, if so it returns false, else true
    /// </summary>
    /// <param name="num">The int to test if prime</param>
    /// <returns>Returns true if num is prime, else false</returns>
    bool isNumberPrime(int num)
    {
        if (num <= 1)
        {
            return false;
        }

        float sqrt = Mathf.Floor(Mathf.Sqrt(num));

        //   Debug.Log("Checking for prime " + num + "::" + sqrt);

        for (float x = sqrt; x >= 2; x--)
        {
            float val = (num / x);
            //Debug.Log(val);
            if (val % 1 == 0)
            {
                return false;
            }
        }


        return true;
    }
}
