﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// A class that stores the ShapeInformation data on the objects created from the json data so its visible in the inspector
/// </summary>
public class ShapeInformationDisplay : MonoBehaviour
{

    public string stringField;
    public int intField;
    public float floatField;
    public bool boolField;

    public void setValues(ShapeInformation toSet)
    {
        stringField = toSet.stringField;
        intField = toSet.intField;
        floatField = toSet.floatField;
        boolField = toSet.boolField;
    }
}
